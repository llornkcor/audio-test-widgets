#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QAudioDevice>
#include <QStyle>
#include <QToolButton>
// https://192.168.1.137/media/sample.wav
// qrc:/sample.wav
// qrc:/organfinale.wav


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    ui->stopButton->setIcon(style()->standardIcon(QStyle::SP_MediaStop));
    ui->pauseButton->setIcon(style()->standardIcon(QStyle::SP_MediaPause));

    mediaDevices = new QMediaDevices;
    connect(mediaDevices, &QMediaDevices::audioInputsChanged,
            [=]() { qWarning() << Q_FUNC_INFO << "audioInputsChanged"; });
    connect(mediaDevices, &QMediaDevices::videoInputsChanged,
            [=]() { qWarning() << Q_FUNC_INFO << "videoInputsChanged"; });
    connect(mediaDevices, &QMediaDevices::audioOutputsChanged,
            [=]() { qWarning() << Q_FUNC_INFO << "audioOutputsChanged"; });


    player = new QMediaPlayer;
    if (player->error()) {
        qWarning() << player->errorString();
        return;
    }
    audioOutput = new QAudioOutput; // listener
    qWarning() << Q_FUNC_INFO << audioOutput->device().description();
    player->setAudioOutput(audioOutput);
//    player->setSource(QUrl("qrc:/organfinale.wav")); // QPlatformMediaPlayer::setMedia

    connect(player, &QMediaPlayer::positionChanged, this, [=] (qint64 position){
        qWarning() << "position changed" << position;
//        ui->positionLabel->setText(QString::number(position));

        ui->progressSlider->setValue(position);

       // player->duration
    });
    connect(player, &QMediaPlayer::errorChanged, this, [=] (){
        if (player->error() == QMediaPlayer::NoError)
            return;
        qWarning() << "errorChanged" << player->errorString();
        //  ui->textEdit->setText(player->errorString());
    });

    connect(player, &QMediaPlayer::mediaStatusChanged, this,
            [=] (QMediaPlayer::MediaStatus status){
        qWarning() << "mediaStatusChanged" << status;
        //    ui->textEdit->setText("new state");
    });

    connect(player, &QMediaPlayer::playbackStateChanged, this,
            [=] (QMediaPlayer::PlaybackState newState){
        qWarning() << "playbackStateChanged" << newState;
        //    ui->textEdit->setText("new state");
    });

    connect(player, &QMediaPlayer::durationChanged, this,
            [=] (qint64 duration){
        qWarning() << "duration changed" << duration;
        ui->progressSlider->setMaximum(duration);
        ui->progressSlider->setValue(0);

//        ui->durationValueLabel->setText(QString::number(duration));
//        ui->progressSlider->setMaximum(duration);
    });

    connect(player, &QMediaPlayer::bufferProgressChanged, this,
            [=] (float filled){
        qWarning() << "buffer progress" << filled;
//        ui->progressBar->setValue(filled * 100);
    });

    connect(player, &QMediaPlayer::metaDataChanged, this,
            [=] (){
        qWarning() << "metadata changed";
    });

    connect(player, &QMediaPlayer::sourceChanged, this,
            [=] (){
        qWarning() << "source changed";
    });



    audioOutput->setVolume(50);
    ui->volumeSlider->setValue(50);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_playButton_clicked()
{
    qWarning() << Q_FUNC_INFO << ui->lineEdit->text();
    if (ui->lineEdit->text().isEmpty()) {
        player->setSource(QUrl("qrc:/sample.wav")); // QPlatformMediaPlayer::setMedia
    } else {
        player->setSource(QUrl(ui->lineEdit->text())); // QPlatformMediaPlayer::setMedia
    }
    player->play();
    isPlaying = true;
}


void MainWindow::on_pauseButton_clicked()
{
    if (isPlaying) {
        player->pause();
        isPlaying = false;
    } else {
        player->play();
        isPlaying = true;
    }
}


void MainWindow::on_stopButton_clicked()
{
    qWarning() << Q_FUNC_INFO;
    player->stop();
}


void MainWindow::on_volumeSlider_sliderReleased()
{
    qWarning() << Q_FUNC_INFO << "<<<<<<<<<<<<<<<<<<<<<<" << ui->volumeSlider->value();
    audioOutput->setVolume((float)ui->volumeSlider->value()/100);
}


void MainWindow::on_progressSlider_valueChanged(int value)
{

}

