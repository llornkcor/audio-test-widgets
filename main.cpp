#include "mainwindow.h"

#include <QApplication>
#include <QLoggingCategory>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QLoggingCategory::setFilterRules("*.debug=false\n"
                                     "qt.multimedia.wasm.*=true");

    MainWindow w;
    w.show();
    return a.exec();
}
